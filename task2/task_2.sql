DROP TABLE IF EXISTS `gender`;
CREATE TABLE IF NOT EXISTS `gender` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` bigint(20) NOT NULL,
  `married_to_with` bigint(20) DEFAULT NULL,
  `have_father` bigint(20) DEFAULT NULL,
  `have_mother` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `married_to_with_fk` FOREIGN KEY (`married_to_with`) REFERENCES `person` (`id`),
  CONSTRAINT `have_father_fk` FOREIGN KEY (`have_father`) REFERENCES `person` (`id`),
  CONSTRAINT `have_mother_fk` FOREIGN KEY (`have_mother`) REFERENCES `person` (`id`),
  CONSTRAINT `gender_fk` FOREIGN KEY (`gender`) REFERENCES `gender` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `seo_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `seo_id_fk` FOREIGN KEY (`seo_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `person_company`;
CREATE TABLE IF NOT EXISTS `person_company` (
  `p_id` bigint(20) NOT NULL,
  `c_id` bigint(20) NOT NULL,
  `position` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,  
  PRIMARY KEY (`p_id`, `c_id`),
  CONSTRAINT `p_id_fk` FOREIGN KEY (`p_id`) REFERENCES `person` (`id`),
  CONSTRAINT `c_id_fk` FOREIGN KEY (`c_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;